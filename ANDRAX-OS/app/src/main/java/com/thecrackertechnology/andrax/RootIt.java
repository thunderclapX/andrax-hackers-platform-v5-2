package com.thecrackertechnology.andrax;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

public class RootIt extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.rootit);

    }

    @Override
    public void onPause() {
        super.onPause();
        finish();
    }

}
